# Serverless Assignment

## Completed Work

**Part 1**

- Implement all 5 lambda functions
- Deploy to API Gateway, export Swagger
- Sign all commits in the repository with a GPG key
- Define a set of Lambda roles

**Part 2**

- Sort tasks based on completion and priority
- Share tasks between users
- Ensure list Lambda is `O(n*log(n))`
- Define a set of Lambda roles
- Email notifications when shared tasks are modified

## Incomplete Work

**Part 2**

- Deploy Task Application with Terraform

### Setup

To set up the project, you'll first need to run `npm install`. Then, you can run local unit tests like so:

```
$ npm install
....
$ npm run test:short

> lambda-task-api@ test:short /home/baker/prog/lambda-task-api
> mocha test/sort_tests.js test/validation_tests.js



 Sorting tests
   ✓ should sort properly

 Validation
   ✓ should allow a valid tasks
   ✓ should disallow invalid tasks


 3 passing (35ms)

```

There is also a set of acceptance tests which interact with the API via the API Gateway. To run them, you'll need to set the `TASK_API_URL` environment variable

```
export TASK_API_URL='https://abc123.execute-api.us-east-1.amazonaws.com/test/tasks'

$ npm test

> lambda-task-api@ test /home/baker/prog/lambda-task-api
> mocha



  Validation
    ✓ should allow a valid tasks
    ✓ should disallow invalid tasks

  Task List API
    Add task
      ✓ should return the task and an ID (1420ms)
      ✓ should disallow invalid tasks (229ms)
    List tasks
      ✓ should list all tasks (343ms)
    Update task
      ✓ should update the task (843ms)
      ✓ should disallow invalid tasks (282ms)
      ✓ should disallow unauthorized user (427ms)
    Delete task
      ✓ should no longer be present in the list (1623ms)
      ✓ should disallow unauthorized user (380ms)
    Share tasks
      ✓ should allow modification when specified (661ms)
      ✓ should disallow modification when specified (495ms)

  Sorting
    ✓ should sort properly


  13 passing (8s)

```

## Explanation of work

### Lambda functions

The Lambdas are implemented by the following functions:

* `addTask` - `index.add`
* `listTasks` - `index.list`
* `updateTask` - `index.update`
* `deleteTask` - `index.delete`
* `sendIncompleteTasksEmails` - `email.handler`

### Swagger file

The API Gateway Swagger definition is stored in `swagger.yml`.

### GPG signed commits

You can view signatures on commits with this command:

```
$ git log --show-signature
commit 6a58bf369b44e2f3b6b26bf38b8ad7c6083e6e7f
gpg: Signature made Fri 03 Feb 2017 02:02:20 AM EST using RSA key ID FC04F9FF
gpg: Good signature from "Baker Bokorney <bbokorney@gmail.com>"
Author: Baker Bokorney <bbokorney@gmail.com>
Date:   Fri Feb 3 02:02:20 2017 -0500

    Add sharing functionality
...
```

### Lambda Roles

A role was created for each Lambda, as can be see in the screenshot.

![Roles](images/roles.png)

### Sorting tasks

Task sorting is done using [`Array.sort`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort) (the code can be seen in `src/sort.js`). Because this is running in a Node.js environment, and Node.js uses the V8 Javascript engine, it's using V8's sorting algorithm. V8's sorting algorithm is [quick-sort](https://github.com/v8/v8/blob/master/src/js/array.js#L709), which has `O(n*log(n))` complexity.

A comparator function is used to determine the application specific ordering, but this comparator runs in constant time. Therefore, the overall time complexity of the `listTasks` Lambda function is `O(n*log(n))`.

### Sharing Tasks

The sharing tasks feature had the requirement that the author of the task could choose to
allow the task to be modifiable by users it was shared with. This implies some sort of
authentication and authorization on the API. I simulated authentication by requiring
the `X-User` HTTP header for all request requiring authentication,
which would be the email of the user who is sending the request.

I also implemented the following authorization checks:

* A user can modify any task they create
* A user can modify a task they did _not_ create if the task has been shared with
them and the author chose to allow them to modify it
* Otherwise, requests to modify a task are denied with `403 Forbidden`

If the `X-User` HTTP header is not present for a request requiring authentication,
the request is denied with `401 Unauthorized`.

I stored the sharing information in the entry in DynamoDB where the task data is stored.


### Emails

#### Incomplete Tasks Email

Below are two images of the incomplete tasks emails created by a single run of the `sendIncompleteTasksEmails` Lambda. Each task description is prefixed by its priority.

These emails were sent to different addresses (`bbokorney@gmail.com` and `bbokorney@ufl.edu`).
For demonstration purposes, the
email address was included in the task descriptions to show that the tasks were
sent to the proper recipient.

You can also see how the tasks are in sorted order of priority.

![Incomplete tasks email](images/incomplete-tasks-email-1.png)
![Incomplete tasks email](images/incomplete-tasks-email-2.png)


#### Shared Task Updated Email

Below is an image of the shared task updated email. This is sent as part of the
`updateTask` Lambda when it detects the change was made by a user who the task
was shared with. The message includes the username of the user who update the task.

![Shared task updated email](images/shared-task-updated-email.png)
