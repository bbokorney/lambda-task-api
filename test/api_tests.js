var expect = require('chai').expect;
var request = require('request');
var rp = require('request-promise');
const testData = require('./validation_tests');
var baseURL;

describe('Task List API', function() {

  this.timeout(10000);

  before(function(done) {
    baseURL = process.env.TASK_API_URL;
    if(!baseURL) {
      throw new Error("Must set TASK_API_URL environment variable");
    }

    deleteAll(done);
  });

  after(function(done) {
    deleteAll(done);
  });

  describe('Add task', function() {
    it('should return the task and an ID', function (done) {
      addTask(task(), function (err, res, body){
        expect(err).to.be.null;
        expect(res.statusCode).to.equal(201);
        assertTaskContents(res.body);
        done();
      });
    });

    it('should disallow invalid tasks', function (done) {
      addTask(invalidTask(), function (err, res, body){
        expect(err).to.be.null;
        expect(res.statusCode).to.equal(400);
        done();
      });
    });
  });

  describe('List tasks', function() {
    before(function() {
      addTask(task(), function(err, res, body){});
      addTask(task(), function(err, res, body){});
      addTask(task(), function(err, res, body){});
    })

    it('should list all tasks', function(done) {
      getTasks(function (err, res, body) {
        expect(err).to.be.null;
        expect(res.statusCode).to.equal(200);
        JSON.parse(res.body).forEach(function(task) {
          assertTaskContents(task);
        });
        done();
      });
    });
  });

  describe('Update task', function() {
    it('should update the task', function(done) {
      addTask(task(), function(err, res, body) {
        expect(err).to.be.null;
        expect(res.statusCode).to.equal(201);
        var taskID = res.body.id;
        var updatedTask = task();
        updatedTask.priority = 3;
        updateTask(taskID, updatedTask, updatedTask.user, function(err, res, body) {
          expect(err).to.be.null;
          expect(res.statusCode).to.equal(200);
          getTaskByID(taskID, function(task) {
            expect(task).to.not.be.null;
            expect(task.priority).to.equal(3);
            done();
          });
        });
      });
    });

    it('should disallow invalid tasks', function(done) {
      addTask(task(), function(err, res, body) {
        expect(err).to.be.null;
        expect(res.statusCode).to.equal(201);
        var taskID = res.body.id;
        var updatedTask = task();
        updatedTask.priority = 30;
        updateTask(taskID, updatedTask, updatedTask.user, function(err, res, body) {
          expect(err).to.be.null;
          expect(res.statusCode).to.equal(400);
          done();
        });
      });
    });

    it('should disallow unauthorized user', function(done) {
      addTask(task(), function(err, res, body) {
        expect(err).to.be.null;
        expect(res.statusCode).to.equal(201);
        var taskID = res.body.id;
        var updatedTask = task();
        updatedTask.priority = 3;
        updateTask(taskID, updatedTask, "someuser@example.com", function(err, res, body) {
          expect(err).to.be.null;
          expect(res.statusCode).to.equal(403);
          done();
        });
      });
    });
  });

  describe('Delete task', function() {
    it('should no longer be present in the list', function(done) {
      addTaskPromise().then(function(resp) {
        var taskID = resp.id;
        deleteTaskPromise(resp.id, resp.task.user).then(function(resp) {
          getTaskByID(taskID, function(task) {
            expect(task).to.be.null;
            done();
          });
        }).catch(function(err) {
          expect(err).to.be.null;
          done();
        });
      }).catch(function(err) {
        expect(err).to.be.null;
        done();
      });
    });

    it('should disallow unauthorized user', function(done) {
      addTask(task(), function(err, res, body) {
        expect(err).to.be.null;
        expect(res.statusCode).to.equal(201);
        var taskID = res.body.id;
        deleteTask(taskID, "someuser@example.com", function(err, res, body) {
          expect(err).to.be.null;
          expect(res.statusCode).to.equal(403);
          done();
        });
      });
    });
  });

  describe('Share tasks', function() {
    it('should allow modification when specified', function(done) {
      sharingTest(true, 200, done);
    });

    it('should disallow modification when specified', function(done) {
      sharingTest(false, 403, done);
    });
  });
});

function sharingTest(allowModify, expectedCode, done) {
  var sharedTask = task();
  sharedTask.sharing = [
    {
      user: "other.user@example.com",
      allowModify: allowModify
    }
  ]
  addTask(sharedTask, function(err, res, body) {
    expect(err).to.be.null;
    expect(res.statusCode).to.equal(201);
    var taskID = res.body.id;
    sharedTask.description = "Updated description by sharing recipient";
    updateTask(taskID, sharedTask, "other.user@example.com", function(err, res, body) {
      expect(err).to.be.null;
      expect(res.statusCode).to.equal(expectedCode);
      done();
    });
  });
}

function assertTaskContents(body) {
  expect(body.id).to.exist;
  expect(body.task.user).to.equal(task().user);
  expect(body.task.description).to.not.be.empty;
  expect(body.task.priority).to.be.defined;
}

function addTask(task, callback) {
  request({
    url: baseURL,
    method: 'POST',
    json: task,
  },
  function (err, res, body){
    callback(err, res, body);
  });
}

function addTaskPromise() {
  return rp({
    url: baseURL,
    method: 'POST',
    json: task()
  });
}

function updateTask(id, task, user, callback) {
  var url = baseURL+'/'+id;
  request({
    url: url,
    method: 'PUT',
    json: task,
    headers: {
      "X-User": user
    },
  },
  function (err, res, body){
    callback(err, res, body);
  });
}

function deleteTaskPromise(id, user) {
  var url = baseURL+'/'+id;
  return rp({
    url: url,
    method: 'DELETE',
    headers: {
      "X-User": user
    },
  });
}

function deleteTask(id, user, callback) {
  var url = baseURL+'/'+id;
  request({
    url: url,
    method: 'DELETE',
    headers: {
      "X-User": user
    },
  },
  function (err, res, body){
    callback(err, res, body);
  });
}

function deleteAll(done) {
  getTasks(function(err, res, body) {
    Promise.all(
      JSON.parse(res.body).map(function(task) {
        return deleteTaskPromise(task.id, task.user);
      })
    ).then(function(arr) {
      done();
    }).catch(function(err) {
      done();
    });
  });
}

function getTasks(callback) {
  request({
    url: baseURL,
    method: 'GET'
  },
  function (err, res, body){
    callback(err, res, body);
  });
}

function getTaskByID(id, callback) {
  getTasks(function(err, res, body) {
    JSON.parse(res.body).forEach(function(elem) {
      if(elem.id === id) {
        callback(elem.task);
      }
      return;
    });
    callback(null);
  });
}

function invalidTask() {
  return testData.invalidTasks()[0];
}

function task() {
  return {
    user: "bbokorney@gmail.com",
    description: "Write some tests",
    priority: 0
  };
}
