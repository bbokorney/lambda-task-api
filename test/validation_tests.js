var expect = require('chai').expect;
var validation = require('../src/validation');

describe('Validation', function() {
  it('should allow a valid tasks', function() {
    validTasks.forEach(function(task) {
      expect(validation.validateTask(task).errors).to.be.empty;
    });
  });

  it('should disallow invalid tasks', function() {
    invalidTasks.forEach(function(task) {
      expect(validation.validateTask(task).errors).to.not.be.empty;
    });
  });
});

exports.invalidTasks = function() {
  return invalidTasks;
};

var validTasks = [
  {
    description: "description",
    user: "user@example.com",
    priority: 1
  },
  {
    description: "description",
    priority: 1,
    completed: "2017-02-02T13:32:17+00:00"
  },
  {
    user: "user@example.com",
    description: "description",
    priority: 1
  },
  {
    description: "description",
    priority: 1,
    completed: "2017-02-02T13:32:17+00:00"
  },
  {
    user: "user@example.com",
    description: "description",
    priority: 1,
    completed: "2017-02-02T13:32:17+00:00",
    sharing: [
      {
        user: "other.user@example.com",
        allowModify: true
      }
    ]
  },
]

var invalidTasks = [
  {
    user: "user@example.com",
    priority: 1,
    completed: "2017-02-02T13:32:17+00:00"
  },
  {
    user: "user at some website example.com",
    description: "description",
    priority: 1
  },
  {
    description: "description",
    priority: 10,
    completed: "2017-02-02T13:32:17+00:00"
  },
  {
    user: "user@example.com",
    description: "description",
    priority: 1,
    completed: "Not a time"
  },
  {
    user: "user@example.com",
    description: "description",
    priority: 1,
    completed: "2017-02-02T13:32:17+00:00",
    sharing: [
      {
        user: "other.user@example.com",
      }
    ]
  },
]
