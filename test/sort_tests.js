var expect = require('chai').expect;
var sortTasks = require('../src/sort').sortTasks;

describe('Sorting', function() {
  it('should sort properly', function() {
    sorted = sortTasks(unsortedTasks);
    expect(sorted).to.deep.equal(sortedTasks);
  });
});

var unsortedTasks = [
  {
    "id": "abc123",
    "task": {
      "user": "testy.mctester@example.com",
      "description": "Do something awesome",
      "priority": 2,
      "completed": "2016-07-06T12:22:46-04:00"
    }
  },
  {
    "id": "abc123",
    "task": {
      "user": "testy.mctester@example.com",
      "description": "Do something awesome",
      "priority": 1,
      "completed": ""
    }
  },
  {
    "id": "abc123",
    "task": {
      "user": "testy.mctester@example.com",
      "description": "Do something awesome",
      "priority": 0,
      "completed": "2016-06-06T12:22:46-04:00"
    }
  },
  { "id": "abc123",
    "task": {
      "user": "testy.mctester@example.com",
      "description": "Do something awesome",
      "priority": 1,
      "completed": "2016-07-06T12:22:46-04:00"
    }
  },
  {
    "id": "abc123",
    "task": {
      "user": "testy.mctester@example.com",
      "description": "Do something awesome",
      "priority": 0,
      "completed": ""
    }
  },
];

var sortedTasks = [
  {
    "id": "abc123",
    "task": {
      "user": "testy.mctester@example.com",
      "description": "Do something awesome",
      "priority": 0,
      "completed": ""
    }
  },
  {
    "id": "abc123",
    "task": {
      "user": "testy.mctester@example.com",
      "description": "Do something awesome",
      "priority": 1,
      "completed": ""
    }
  },
  { "id": "abc123",
    "task": {
      "user": "testy.mctester@example.com",
      "description": "Do something awesome",
      "priority": 1,
      "completed": "2016-07-06T12:22:46-04:00"
    }
  },
  {
    "id": "abc123",
    "task": {
      "user": "testy.mctester@example.com",
      "description": "Do something awesome",
      "priority": 2,
      "completed": "2016-07-06T12:22:46-04:00"
    }
  },
  {
    "id": "abc123",
    "task": {
      "user": "testy.mctester@example.com",
      "description": "Do something awesome",
      "priority": 0,
      "completed": "2016-06-06T12:22:46-04:00"
    }
  },
]
