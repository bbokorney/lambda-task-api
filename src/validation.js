var Validator = require('jsonschema').Validator;

exports.validateTask = function(task) {
  var v = new Validator();
  return v.validate(task, schema);
};

var schema = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "definitions": {
    "user": {
      "type": "string",
      "format": "email",
      "minLength": 5,
      "maxLength": 254,
      "title": "User",
      "description": "User's email address"
    }
  },

  "type": "object",
  "title": "A task",
  "properties": {
    "user": {
      "$ref": "#/definitions/user"
    },
    "description": {
      "type": "string",
      "minLength": 1,
      "title": "Description of the task"
    },
    "priority": {
      "type": "integer",
      "multipleOf": 1,
      "maximum": 9,
      "minimum": 0,
      "title": "Priority",
      "description": "Task priority, as a single-digit integer. 0 is highest priority"
    },
    "completed": {
      "type": "string",
      "format": "date-time",
      "title": "Completed",
      "description": "Completed datetime, formatted as an ISO8601 string"
    },
    "sharing": {
      "type": "array",
      "title": "Sharing",
      "description": "Info about sharing",
      "items": {
        "type": "object",
        "properties": {
          "user": {
            "$ref": "#/definitions/user"
          },
          "allowModify": {
            "type": "boolean"
          }
        },
        "required": [
          "user",
          "allowModify"
        ]
      }
    },
  },
  "required": [
    "description",
    "priority"
  ],
}
