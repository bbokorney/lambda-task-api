exports.sortTasks = function(tasks) {
  return tasks.sort(function(item1, item2) {
    var a = normalizeItem(item1);
    var b = normalizeItem(item2);

    // first compare completed times
    var dateA = new Date(a.completed);
    var dateB = new Date(b.completed);
    // if their completed date's aren't equal
    if(dateA.getTime() != dateB.getTime()) {
      // compared by completed date
      return dateB.getTime() - dateA.getTime();
    }

    // completed dates are equal, compare by priority
    return a.priority - b.priority;
  });
};

function normalizeItem(item) {
  var ret = {};
  Object.assign(ret, item.task)
  return normalizeDate(ret);
}

// this date should be in the future for a while...
const futureDate = "5000-01-01T00:00:00+00:00"
function normalizeDate(task) {
  if(!task.completed) {
    task.completed = futureDate;
  }
  return task;
}
