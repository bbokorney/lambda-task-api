const AWS = require('aws-sdk');
const ses = new AWS.SES({region: 'us-east-1'});
const listTasks = require('./index').listTasks;
const sortTasks = require('./sort').sortTasks;

exports.handler = (event, context, callback) => {
  listTasks(function(err, data) {
    if(err) {
      callback(err);
      return;
    }

    // filter out tasks which have been completed
    // and have no user
    incompleteTasks = filterNoUser(filterCompletedTasks(data.Items));
    // create a mapping of user => [tasks]
    userMapping = createUserMapping(incompleteTasks);
    console.log(mapping);

    for (var user in userMapping) {
      if (userMapping.hasOwnProperty(user)) {
        sendEmail(user, userMapping[user], function(err, data) {
          callback(err, data);
        });
      }
    }
  });
};

function createUserMapping(tasks) {
  mapping = {};
  tasks.forEach(function(item) {
    if(!mapping[item.task.user]) {
      mapping[item.task.user] = [];
    }
    mapping[item.task.user] = mapping[item.task.user].concat(item);
  });

  return mapping;
}

function filterCompletedTasks(tasks) {
  return tasks.filter(function(item) {
    return item.task.completed == undefined;
  });
}

function filterNoUser(tasks) {
  return tasks.filter(function(item) {
    return item.task.user != undefined;
  });
}

function formatTaskList(tasks) {
  tasks = sortTasks(tasks);
  var listString = "";
  tasks.forEach(function(item) {
    listString += "\n"+" - "+item.task.priority+" | ";
    listString += item.task.description;
  });
  return listString;
}

function sendEmail(recipient, tasks, callback) {
  var params = {
    Destination: {
      ToAddresses: [recipient]
    },
    Message: {
      Body: {
        Text: {
          Data: "Hello there!\n\nHere's a list of things you need to do.\n"+formatTaskList(tasks)
        }
      },
      Subject: {
        Data: "Your Incomplete Tasks"
      }
    },
    Source: "bbokorney@gmail.com"
 };


  ses.sendEmail(params, function(err, data) {
    callback(err, data);
  });
}

exports.sendTaskModifiedEmail = function(item, modifier, callback) {
  var task = item.task;
  var params = {
    Destination: {
      ToAddresses: [task.user]
    },
    Message: {
      Body: {
        Text: {
          Data: "Hello there!\n\nOne of your shared tasks was updated by "+modifier+".\n\n"+formatTask(task)
        }
      },
      Subject: {
        Data: "Your Shared Task Was Updated"
      }
    },
    Source: "bbokorney@gmail.com"
 };


  ses.sendEmail(params, function(err, data) {
    callback(err, data);
  });
};

function formatTask(task) {
  var text = "Description: " + task.description;
  text += "\nPriority: " + task.priority;
  if(task.completed) {
    text += "\nCompleted: " + task.completed;
  }

  return text;
}
