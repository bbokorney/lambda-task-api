const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient({region: "us-east-1"});
const uuidV4 = require('uuid/v4');
const tableName = 'tasks';
const userHeader = 'X-User';
const validation = require('./validation');
const email = require('./email');
const sortTasks = require('./sort').sortTasks;

function putTask(id, task, callback) {
  var taskData = {
    id: id,
    task: task
  }
  var params = {
    Item: taskData,
    TableName: tableName
  }

  docClient.put(params, function(err, data) {
    if(err) {
      callback(err);
    } else {
      callback(null, taskData);
    }
  });
}

function listTasks(callback) {
  var params = {
    TableName: tableName
  };

  docClient.scan(params, function(err, data) {
    callback(err, data);
  });
}

function getTaskByID(id, callback) {
  var params = {
    TableName: tableName,
    Key: {
      id: id
    }
  };

  docClient.get(params, function(err, data) {
    callback(err, data);
  });
}

function deleteTask(id, callback) {
  var params = {
    Key: {
      id: id
    },
    TableName: tableName
  }

  docClient.delete(params, function(err, data) {
    if(err) {
      callback(err);
    } else {
      callback(null, data);
    }
  });
}

exports.add = (event, context, callback) => {
  errors = validation.validateTask(event).errors;
  if(errors.length > 0) {
    callback("BadRequest "+errors.toString());
    return;
  }

  var id = uuidV4();
  var task = event;
  putTask(id, task, callback);
};

exports.list = (event, context, callback) => {
  listTasks(function(err, data) {
    if(err) {
      callback(err);
    } else {
      callback(null, sortTasks(data.Items));
    }
  });
};

exports.delete = (event, context, callback) => {
  var user = event.params.header[userHeader];
  var id = event.params.path.id;

  if(!user) {
    callback("Unauthorized Missing X-User header");
    return;
  }

  getTaskByID(id, function(err, data) {
    if(err) {
      callback(err);
    } else {
      if(userCanModify(user, data.Item)) {
        deleteTask(id, callback);
      } else {
        callback("Forbidden")
      }
    }
  });
};

exports.update = (event, context, callback) => {
  var id = event.params.path.id;
  var task = event['body-json'];
  var user = event.params.header[userHeader];

  if(!user) {
    callback("Unauthorized Missing X-User header");
    return;
  }

  errors = validation.validateTask(task).errors;
  if(errors.length > 0) {
    callback("BadRequest "+errors.toString());
    return;
  }

  getTaskByID(id, function(err, data) {
    if(err) {
      callback(err);
    } else {
      var item = data.Item;
      if(userCanModify(user, item)) {
        putTask(id, task, function(err, data) {
          if(err) {
            callback(err);
            return;
          }
          // send a notification to the author if a shared
          // task is modified
          if(user != item.task.user) {
            email.sendTaskModifiedEmail(item, user, function(err, data) {
              callback(null, data.Item);
            });
          }
        });
      } else {
        callback("Forbidden")
      }
    }
  });
};

function userCanModify(user, item) {
  var task = item.task;
  // if there is no user, no one can modify this task
  if(!task.user) {
    return false;
  }

  // a user can modify their own task
  if(task.user == user) {
    return true;
  }

  // if there's no sharing, then
  // the user can't modify the task
  if(!task.sharing) {
    return false;
  }

  var allowed = false;
  task.sharing.forEach(function(item) {
    // if this task has been shared with this
    // user and they can modify
    if(item.user == user && item.allowModify) {
      allowed = true;
    }
  });

  return allowed
}

exports.listTasks = (callback) => {
  listTasks(callback);
};
