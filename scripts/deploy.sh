#!/bin/sh

mkdir -p dist

cp -r package.json src/*.js dist/

cd dist

npm install --only=production

zip -r funcs.zip *.js node_modules/

deploy_list='addTask listTasks deleteTask updateTask sendIncompleteTasksEmails'

if [ "$#" -ne 0 ]; then
  deploy_list="$@"
fi

echo Deploying $deploy_list

for func_name in $deploy_list; do
  aws lambda update-function-code \
    --function-name $func_name \
    --zip-file fileb://funcs.zip
done
