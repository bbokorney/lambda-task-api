#!/bin/sh

count=3

if [ ! -z "$1" ]; then
  count=$1
else
  echo "Adding $count tasks"
fi

for i in $(seq 1 $count); do
  random_index=$(shuf -i 0-9 -n 1)
  aws lambda invoke \
    --function-name addTask \
    --payload file://testdata/task-$random_index.json \
    lambda-output.txt
done

aws lambda invoke \
  --function-name listTasks \
  lambda-output.txt

cat lambda-output.txt | jq
